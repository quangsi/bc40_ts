console.log("Hello TS");
// basic type
// var name:type =value
var age = 6;
// age = "5" error;
age = 8;
var username = "alice";
var isHoliday = true;
// type any: chấp nhận mọi giá trị
var value = "alice";
value = 2;
value = true;
var isMarried = null;
var is_married = undefined;
// type function
function tinhTong(toan, van) {
  return toan + van;
}
// void => không có giá trị trả về
function gioiThieuSanPham(name) {
  console.log(name);
}
// advance type
var scores = [1, 4, 88];
var colors = ["black", "blue"];
var alice = {
  name: "alice nguyen",
  age: 2,
  email: "alice@gmail.com",
};
var bob = {
  name: "bob",
  age: 2,
  email: "bob@gmail.com",
};
var todo1 = {
  id: 1,
  title: "Làm capsone React nha",
  isDone: false,
};
var todo2 = {
  id: 2,
  title: "Làm dự án cuối khoá nha",
  isDone: false,
  timeCreated: "6/5/2023",
};
var Animals = /** @class */ (function () {
  function Animals(_name, _legs) {
    this.name = _name;
    this.legs = _legs;
  }
  Animals.prototype.introduce = function () {
    console.log(" say hello ", this.name);
  };
  return Animals;
})();
var animal1 = new Animals("lulu", 2);
console.log(name);
