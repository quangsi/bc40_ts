console.log("Hello TS");

// basic type

// var name:type =value
var age: number = 6;
// age = "5" error;
age = 8;
var username: string = "alice";
var isHoliday: boolean = true;
// type any: chấp nhận mọi giá trị
var value: any = "alice";
value = 2;
value = true;
var isMarried: null = null;
var is_married: undefined = undefined;

// type function

function tinhTong(toan: number, van: number): number {
  return toan + van;
}
// void => không có giá trị trả về
function gioiThieuSanPham(name: string): void {
  console.log(name);
}

// advance type

var scores: number[] = [1, 4, 88];
var colors: string[] = ["black", "blue"];

// interface : mô tả loại giữ liệu của key mà object chứa

interface SinhVien {
  name: string;
  age: number;
  email: string;
  phone?: number;
  //   optional property
}

let alice: SinhVien = {
  name: "alice nguyen",
  age: 2,
  email: "alice@gmail.com",
};

let bob: SinhVien = {
  name: "bob",
  age: 2,
  email: "bob@gmail.com",
};

interface Todo {
  id: number;
  title: string;
  isDone: boolean;
  desc?: string;
}

let todo1: Todo = {
  id: 1,
  title: "Làm capsone React nha",
  isDone: false,
};

interface Todov2 extends Todo {
  timeCreated: string;
}

let todo2: Todov2 = {
  id: 2,
  title: "Làm dự án cuối khoá nha",
  isDone: false,
  timeCreated: "6/5/2023",
};

class Animals {
  private name: string;
  legs: number;
  constructor(_name: string, _legs: number) {
    this.name = _name;
    this.legs = _legs;
  }
  introduce() {
    console.log(" say hello ", this.name);
  }
}
let animal1 = new Animals("lulu", 2);

class Dog extends Animals {
  type: string;
  constructor(_name: string, _legs: number, _type: string) {
    super(_name, _legs);
    this.type = _type;
  }
}
let dog1 = new Dog("milo", 4, "husky");

// generic

// let nums: number[] = [1, 4, 6];
let nums: Array<number> = [1, 4, 6];
let colorss: Array<string> = ["red", "blue"];

// <> genric

// function useState(initialValue: number) {
//   let state: number = initialValue;
//   function getState() {
//     return state;
//   }
//   function setState(newValue: number) {
//     state = newValue;
//   }
//   return [getState, setState];
// }
// let [like, setLike] = useState(100);
// setLike(200);

function useState<T>(initialValue: T) {
  let state: T = initialValue;
  function getState() {
    return state;
  }
  function setState(newValue: T) {
    state = newValue;
  }
  return [getState, setState];
}
let [like, setLike] = useState<string>("1");
setLike("100");
